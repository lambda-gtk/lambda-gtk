(in-package :cl-user)

(defpackage :lambda-gtk
  (:use :cl
        #+(and openmcl (not cffi-lambda-gtk)) :ccl
        #+(and sbcl (not cffi-lambda-gtk)) :sb-alien
        #+(and cmu (not cffi-lambda-gtk)) :alien
        #+cffi-lambda-gtk :cffi)
  (:export #:lambda-gtk))
