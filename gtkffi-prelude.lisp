;;;
;;; Gtk lib loading. *gtk-libdir* must point to where GTK's libs are
;;; located and *gtk-libfiles* better agree with whatever the unix
;;; command 'pkg-config gtk+-2.0 --libs' returns on your system.

(in-package :lambda-gtk)

(eval-when (:compile-toplevel :load-toplevel :execute)
  #+(and sbcl (not cffi-lambda-gtk))
  (setq sb-alien::*values-type-okay* t)
  (export '(*gtk-libdir* *gtk-libfiles*) :lambda-gtk)
  (defvar *gtk-libdir*
          #+:darwin "/sw/lib/"
          #+:unix "/usr/lib/"
          #+:win32 "C:\\Program Files\\Common Files\\GTK\\2.0\\bin\\")
  (defvar *gtk-libfiles*
    #+win32
    '("libgtk-win32-2.0-0" "libgdk-win32-2.0-0" "libatk-1.0-0" "libgdk_pixbuf-2.0-0"
      "libpangoft2-1.0-0" "libpangowin32-1.0-0" "libpango-1.0-0"
      "libgobject-2.0-0" "libgmodule-2.0-0" "libglib-2.0-0" "intl" "iconv")
    #-win32
    '("libgtk-x11-2.0" "libgdk-x11-2.0" "libatk-1.0"
      "libgdk_pixbuf-2.0"
      #-:darwin "libm" #+:darwin "/usr/lib/libm"
      "libpangoxft-1.0" "libpangox-1.0" "libpango-1.0"
      "libgobject-2.0" "libgmodule-2.0"
      #-:darwin "libdl"
      "libglib-2.0"
      #+:darwin "libintl"
      #+:darwin "libiconv"
     ))
  (flet ((libpath (lib &aux p)
           (setq p (namestring
                     (merge-pathnames (format nil "~A.~A"
                                              lib
                                              #+:darwin "dylib"
                                              #+:unix "so"
                                              #+:win32 "dll")
                                      *gtk-libdir*)))
           (if (probe-file p)
             p
             (error "Library ~S not found. Either GTK is not installed or else lambda-gtk:*gtk-libdir* needs to be set to the directory containing GTK on your machine." p))))
  #+(and sbcl (not cffi-lambda-gtk))
  (dolist (l *gtk-libfiles*) (load-shared-object (libpath l)))
  #+(and cmu (not cffi-lambda-gtk))
  (dolist (l *gtk-libfiles*) (ext:load-foreign (libpath l)))
  #+cffi-lambda-gtk
  (dolist (l *gtk-libfiles*) (cffi:load-foreign-library (libpath l)))))
