;;; -*- Lisp -*-

;; Generate gtk ffi by doing:
;; (require :asdf)
;; (push "/path/to/lambda-gtk/" asdf:*central-registry*)
;; (asdf:operate 'asdf:load-op :lambda-gtk)
;; In openmcl you must preinstall gtk libs and darwin-headers;gtk2;

(in-package #:cl-user)

(defpackage #:lambda-gtk-system
  (:use #:cl
        #:asdf))

(in-package #:lambda-gtk-system)

(pushnew :cffi-lambda-gtk *features*)

(defclass generated-file (cl-source-file) ())
(defmethod perform :around ((o compile-op) (c generated-file))
  (let ((*package* (find-package :lambda-gtk))
        (filename #+(and (or sbcl cmu) (not cffi-lambda-gtk))
                  "gtkffi-cmusbcl"
                  #+cffi-lambda-gtk
                  "gtkffi-cffi"
                  #+(and openmcl (not cffi-lambda-gtk))
                  "gtkffi-openmcl")
        (*print-level* nil)
        (*print-length* nil)
        (*print-lines* nil)
        (directory (asdf:component-pathname (asdf:find-system 'lambda-gtk))))
    (funcall (intern "LAMBDA-GTK" (find-package :lambda-gtk))
             (make-pathname :device (pathname-device directory)
                            :directory (pathname-directory directory)
                            :name filename
                            :type "lisp")))
  (call-next-method))

(defsystem #:lambda-gtk
  :version "0.2"
  :components ((:file "packages")
               (:file "lambda-gtk-common" :depends-on ("packages"))
               #+cffi-lambda-gtk
               (:file "lambda-gtk-cffi"
                      :depends-on ("lambda-gtk-common"))
               #+(and (or cmu sbcl) (not cffi-lambda-gtk))
               (:file "lambda-gtk-cmusbcl"
                      :depends-on ("lambda-gtk-common"))
               #+(and openmcl (not cffi-lambda-gtk))
               (:file "lambda-gtk-openmcl"
                      :depends-on ("lambda-gtk-common"))
               #+(or cmu sbcl cffi-lambda-gtk)
               (:file "gtkffi-prelude"
                      :depends-on ("packages"))
               #+cffi-lambda-gtk
               (:generated-file "gtkffi-cffi"
                                :depends-on ("gtkffi-prelude"
                                             "lambda-gtk-cffi"))
               #+(and (or cmu sbcl) (not cffi-lambda-gtk))
               (:generated-file "gtkffi-cmusbcl"
                                :depends-on ("gtkffi-prelude"
                                             "lambda-gtk-cmusbcl"))
               #+(and openmcl (not cffi-lambda-gtk))
               (:generated-file "gtkffi-openmcl"
                                :depends-on ("lambda-gtk-openmcl")))
  #+cffi-lambda-gtk
  :depends-on
  #+cffi-lambda-gtk
  (#:cffi))
