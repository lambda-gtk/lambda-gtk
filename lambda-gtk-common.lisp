;;; common file for all ffis.

(in-package :lambda-gtk)

(defparameter *lambda-gtk-directory*
  (asdf:component-pathname (asdf:find-system 'lambda-gtk)))

(defparameter lib-packaging t)    ; split libs into packages?
(defparameter gtk-api nil)        ; entries from .api file
(defparameter gtk-ffi nil)        ; entries fron .ffi file
(defparameter gtk-entries '())    ; sorted list of defs
(defparameter gtk-hashnum 0)      ; order of def in output file.
(defparameter gtk-gluecode nil)   ; helper functions, etc.
(defparameter gtk-exports nil)    ; exported symbol lists
(defparameter gdk-exports nil)
(defparameter g-exports nil)
(defparameter atk-exports nil)
(defparameter pango-exports nil)

(defparameter banner-string
";;; This software is released under the Lisp Lesser Gnu Public License
;;; (LLGPL). See http://opensource.franz.com/preamble.html for the
;;; terms of this agreement.
")
(defparameter prelude-string "")   ; printed at start of output file
(defparameter postlude-string "")  ; printed at end of file

(defun gtk-ffi-init ()
  (setf gtk-api (make-hash-table :test #'equal))
  (setf gtk-ffi (make-hash-table :test #'equal))
  (setf gtk-hashnum 0)
  (setf gtk-entries (list))
  t)

(defstruct (ffi-entry (:type list))
  name       ; cstring name
  number     ; index in .ffi file for sorting
  include    ; output to file (nil, :api or t )
  def        ; def from .ffi file
  data)      ; whatever.

(defun get-entry (name)
  (gethash name gtk-ffi))

(defun get-def (name)
  (let ((e (gethash name gtk-ffi)))
    (if e (ffi-entry-def e) nil)))

(defun gtk-banner (fil)
  (multiple-value-bind (sec min hour day mo year)
      (get-decoded-time)
    (format fil ";;; Output by lambda-gtk on ~a.~a.~a at ~a:~a:~s~%"
            day mo year hour min sec)
    (format fil banner-string)))

(defmacro outfile ((fil file) &body body)
  `(with-open-file (,fil ,file :direction :output
                    :if-does-not-exist :create
                    :if-exists :supersede)
    ,@body))

(defun mapdefs (fn file &optional (allowtypes t))
  ;; map fn over expressions in file. used to load .ffi and .api file
  (let ((allow (if (consp allowtypes)
                 allowtypes
                 (if (eq allowtypes t) t
                     (list allowtypes)))))
  (with-open-file (f file)
    (loop for expr = (read f nil :eof)
          until (eql expr ':eof)
          do
          (if (and (consp expr)
                   (or (eq allow t)
                       (find (car expr) (the list allow))))
            (funcall (the function fn) expr))))))

(defun lisp-name (cname &optional (lowercase t))
  ;; translate c name to lisp name.
  (substitute #\- #\_ (if lowercase
                        (string-downcase cname)
                        cname)))
;;;
;;; "accessors" for .ffi defs

(defun def-name (def &optional name)
  (let* ((n (or name (third def)))
         (p (position #\space (the string n))))
    (if p (subseq (the string n) 0 p) n)))
(defun def-data (def)
  (fourth def))
(defun struct-def? (x)
  (if (member (car x) '(struct union)) x nil))
(defun type-def? (x)
  (if (eql (car x) 'type) x nil))
(defun struct-type-def? (x)
  (and (type-def? x)
       (if (member (first (fourth x)) '(struct-ref union-ref))
         (struct-def? (get-def (second (fourth x))))
         ;; GtkAllocation=GdkRectangle
         (struct-def? (get-def (second (fourth x)))))))
(defun function-def? (x)
  (if (eql (car x) 'function) x nil))
(defun macro-def? (x)
  (if (eql (car x) 'macro) x nil))
(defun enum-def? (x)
  (if (eql (car x) 'enum) x nil))
(defun pointer-def? (def)
  (eql (car (fourth def)) 'pointer))
(defun enum-ref-def? (def)
  (eql (car (fourth def)) 'enum-ref))

(defun internal-name? (string)
  (declare (string string))
  (char= (elt string 0) #\_))

(defun upper-case-name? (string)
  (declare (string string))
  (loop for i below (length string)
        for c = (elt string i)
        if (and (alpha-char-p c) (lower-case-p c))
        return nil
        finally (return t)))

;;;
;;; struct frobbing

(defun mapslots (def fun)
  ;; ("slotname" (field (XXX) pos wid))
  (let ((bitf nil)
        (bitw 0))
    (dolist (s (fourth def))
      (cond ((eql (first (second s)) 'field)
             (setq bitf nil bitw 0)
             (funcall fun (first s) (second (second s)) nil))
            ((eql (first (second s)) 'bitfield)
             (if (not bitf) (setq bitf 0))
             (setq bitw (fourth (second s)))
             (funcall fun (first s) (second (second s))
                      (list bitf bitw))
             (incf bitf bitw))
            (t (format t "~%unknown slot type: ~S" s))))))

(defun struct-type? (typ)
  (cond ((null typ) nil)
        ((stringp typ)
         ;;***
         (let ((n (if (internal-name? typ)
                    (subseq typ 1)
                    typ)))
           (struct-type? (get-def n))))
        ((member (car typ) '(struct union))
         typ)
        ((member (car typ) '(struct-ref union-ref))
         ;;***
         (let ((n (if (internal-name? (cadr typ))
                    (subseq (cadr typ) 1)
                    (cadr typ))))
           (get-def n)))
        ((eql (car typ) 'typedef)
         (struct-type? (cadr typ)))
        ((eql (car typ) 'type)
         (struct-type? (fourth typ)))
        (t nil)))

; (struct-type? "_GdkRectangle")
; (struct-type? "GdkRectangle")
; (struct-type? '(struct-ref "_GdkRectangle"))
; (struct-type? '(typedef "GdkRectangle"))
; (struct-type? "GtkAllocation")
; (struct-type? '(typedef "GtkAllocation"))

(defun struct-slot? (name type)
  ;; return struct if  referenced in slot type.
  name
  ;;(print (list name type))
  (case (car type)
    (( pointer ) nil)
    (( typedef struct-ref union-ref )
     (struct-type? (cadr type)))
    (( array )
     (struct-slot? name (third type)))
    (t nil)))

(defun struct-accessor-defs (entry &optional owner arry?)
  #+sbcl
  (declare (sb-ext:muffle-conditions style-warning
                                     sb-ext:compiler-note))
  ;; owner is nil or a list (owning_struct (slot type)) for creating
  ;; the accessors of an included slot
  (let* ((name (ffi-entry-name entry))
         (accs (or (assoc ':accessors
                          (cddr (gethash name gtk-api)))
                   t)))
    (if (or (eql accs t) (cdr accs))
      (let* ((def (ffi-entry-def entry))
             (slots (fourth def))
             (incls (cdr (assoc ':included (ffi-entry-data entry))))
             (funcs (list)))
        ;; never make accessors for a structs's first slot if it is an
        ;; included struct.
        (when (find (first (first slots)) incls :test #'equal
                    :key #'first)
          (setq slots (cdr slots)))
        (dolist (s slots)
          ;; skip all bitfields and slots not on :accessors list
          ;; if it was specified in the api file.
          (when (and (not (eql (first (second s)) 'bitfield))
                     (or (eql accs t)
                         (find (first s) (cdr accs) :test #'equal)))
            (let* ((nam (first s))           ; slot name
                   (typ (second (second s))) ; slot typ
                   (wid (fourth (second s)))
                   ;; inc is nil or included info: ("slot" (struct...))
                   (inc (find nam incls :test #'equal :key #'first))
                   )
              (cond
                (owner
                 ;; we are being called recursively to create
                 ;; accessors for owner's slot
                 (push (struct-slot-accessor (first owner)
                                             arry?
                                             (second owner)
                                             (list nam typ wid))
                       funcs))
                (inc
                 ;; generate recursive call to return included struct's
                 ;; accessors
                 (let ((ary? nil)
                       (subs nil))
                   (if (eql (car typ) 'array)
                     (setq ary? (second typ) typ (third typ)))
                   (setq subs
                         (struct-accessor-defs (get-entry
                                                (def-name (second inc)))
                                               (list name (list nam typ wid))
                                               ary?))
                   ;; re-reverse to preserve original order...
                   (setq funcs (append (nreverse subs) funcs))))

                (t
                 (let ((ary? nil))
                   ;; if an array is being accessed pass the dimension
                   ;; and set the slot type to the array type
                   (if (eql (car typ) 'array)
                     (setq ary? (second typ) typ (third typ)))
                   (push (struct-slot-accessor name ary?
                                               (list nam typ wid))
                         funcs)))))))
        (if funcs
          (nreverse funcs)
          nil))
      nil
    )))

(defun array-slot? (s)
  (eql (car (second (second s))) 'array))

;;;
;;; CSSYM: Case Sensitive Symbol, or C Style Symbol, whatever. these
;;; objects hold strings which are printed as symbols but with their
;;; case preserved in the output file for legibility.

(defun gtk-prefix (str &optional (beg 0))
  ;; str is lisp name in api, possibly with inital * or +
  (let ((len (length str)))
    (cond ((find #\. (the string str))
           ;; if a dot is in name its an accessor
           (cond ((string= str "Gtk" :end1 3)
                  (concatenate 'string "gtk::" (subseq str 3)))
                 ((string= str "Gdk" :end1 3)
                  (concatenate 'string "gdk::" (subseq str 3)))
                 ((string= str "Atk" :end1 3)
                  (concatenate 'string "atk::" (subseq str 3)))
                 ((string= str "G" :end1 1)
                  (concatenate 'string "g::" (subseq str 1)))
                 ((string= str "Pango" :end1 5)
                  (concatenate 'string "pango::" (subseq str 5)))
                 (t str)))
          ((and (> len (+ beg 2))
                (string= str "g-" :start1 beg :end1 (+ beg 2)))
           (concatenate 'string "g::" (subseq str (+ beg 2))))
          ((and (> len (+ beg 4))
                (or (string= str "gtk-" :start1 beg
                             :end1 (+ beg 4))
                    (string= str "gdk-" :start1 beg
                             :end1 (+ beg 4))
                    (string= str "atk-" :start1 beg
                             :end1 (+ beg 4))))
           (concatenate 'string (subseq str beg (+ beg 3))
                        "::" (subseq str (+ beg 4))))
          ((and (> len 6)
                (string= str "pango-" :start1 beg
                         :end1  (+ beg 6)))
           (concatenate 'string "pango::"
                        (subseq str (+ beg 6))))
          ((member (elt str beg) '(#\* #\+))
           ;; we process lisp names in gluecode too.
           (let* ((s (gtk-prefix str (+ beg 1)))
                  (p (position #\: s :from-end t)))
             (if p
               (format nil "~A~C~A" (subseq s 0 (1+ p))
                       (elt str 0)
                       (subseq s (1+ p)))
               str)))
          (t str))))

(defclass cssym () ((str :initarg :name :initarg :string)))

(defmethod print-object ((str cssym) stream)
  (let ((str (slot-value str 'str)))
    (if lib-packaging
        (format stream "~A" (gtk-prefix str))
        (format stream "~A"  str))))

;;;
;;; cssym-list! replaces strings in an ffi output list with cssym
;;; objects, which preserve case and print package prefixes.

(defun cssym-list! (l)
  ;; replace strings in expr l with css objects.
  (if (consp l)
    (if (stringp (car l))
      (progn (setf (car l) (make-instance 'cssym :name (car l)))
             (cssym-list! (cdr l))
             l)
      (progn (cssym-list! (car l))
             (cssym-list! (cdr l))
             l))
    nil))

(defun entry-before? (n1 n2)
  ;; return t if n1 should be output before n1
  ;; all types that do not references structs have negative
  ;; entry numbers so they always appear in the file before
  ;; struct references that use them.
  (if (<= n1 0)
    (if (<= n2 0)
      (> n1 n2)
      (< n1 n2))
    (< n1 n2)))

(defun gtk-entries ()
  ;; map hashtable checking inclusion and sorting by
  ;; original order in .ffi file.
  (let ((defs (list)))
    (maphash #'(lambda (k v)
                 k
                 (if (ffi-entry-include v) (push v defs)))
             gtk-ffi)
    (setq defs (sort defs #'(lambda (x y)
                              (entry-before? (ffi-entry-number x)
                                             (ffi-entry-number y)))))
    defs))

(defun struct-accessor-name (name &rest slots)
  (apply #'concatenate
         'string
         (lisp-name name nil)
         (loop for s in slots collect "." collect (lisp-name s nil))))

(defun add-def (name def)
  ;;  (print (list :adding-> name def))
  (let ((e (gethash name gtk-ffi)))
    ;; replace data in existing entry (_struct )
    (if e
      (progn (setf (ffi-entry-def e) def)
             e)
      (progn (incf gtk-hashnum)
             (setf (gethash name gtk-ffi)
                   (make-ffi-entry :name name
                                   :number gtk-hashnum
                                   :def def))))))

;;;
;;; common routines for entry processing

(defparameter ctype-mappings nil)

(defun gtk-double (c) `(coerce ,c 'double-float))
(defun gtk-single (c) `(coerce ,c 'single-float))
(defun gtk-boolean (c) `(if ,c (if (eq ,c 0) 0 1) 0))

(defun ctype? (x)
  ;; return sbcl's alien type designator for ffi type
  (let ((e (find x (the list ctype-mappings)
                 :test #'equal :key #'first)))
    (if e (cadr e) nil)))

(defun doapi (def)
  ;; load an entry from api file.
  (setf (gethash (second def) gtk-api) def))

(defun dodef (def)
  ;; load an expr from the .ffi
  (case (car def)
    (( function ) (add-def (def-name def) def ))
    (( macro ) (add-def (def-name def) def ))
    (( struct union )
     ;; strip underbar from name add def in replace of its typedef
     (let ((nam (if (internal-name? (third def))
                  (subseq (third def) 1) (third def))))
       (add-def nam def)
       ;; remove _ from actual def too.
       (setf (third def) nam)))
    (( type )
     ;; add type that are NOT structs or unions...
     (unless (member (first (fourth def)) '(struct-ref union-ref ))
       ;; look up the  enum and link it to its typedef
       (when (enum-ref-def? def)
         (let ((en (get-entry (second (fourth def)))))
           (unless en
             (warn "enum type ~S has no enum!" (def-name def)))
           (push (list :type (def-name def))
                 (ffi-entry-data en))))
       (add-def (def-name def) def)))
    (( enum )
     (add-def (def-name def) def))))

(defun gtk-export (str &optional pkg)
  (if (not lib-packaging)
    (push str gtk-exports)
    (let (obj dot)
      (if (symbolp str)
        (setq str (string str))
        (if (typep str 'cssym)
          (setq obj str str (slot-value str 'str))
          (unless (stringp str)
            (error "~S not string, symbol or cssym." str))))
      (unless pkg
        ;; see if we have an accessor
        (setq dot (find #\. (the string str)))
        (cond ((or (string= str "gtk-" :end1 4)
                   (and dot (string= str "Gtk" :end1 3)))
               (setq pkg ':gtk))
              ((or (string= str "gdk-" :end1 4)
                   (and dot (string= str "Gdk" :end1 3)))
               (setq pkg ':gdk))
              ((or (string= str "atk-" :end1 4)
                   (and dot (string= str "Atk" :end1 3)))
               (setq pkg ':atk))
              ((or (string= str "pango-" :end1 6)
                   (and dot (string= str "Pango" :end1 5)))
               (setq pkg ':pango))
              ((or (string= str "g-" :end1 2)
                   (and dot (string= str "G" :end1 1)))
               (setq pkg ':g))))
      (unless obj (setq obj (make-instance 'cssym :name str)))
      (ecase pkg
        (:gtk (push obj gtk-exports))
        (:gdk (push obj gdk-exports))
        (:g (push obj g-exports))
        (:atk (push obj atk-exports))
        (:pango (push obj pango-exports)))))
  (values))

;;; "gluecode" is handcoded definitions that are either used by the
;;; interface or are exported as "helpers" by the api. the helper
;;; gluecode should follow the same naming convensions as the genererated
;;; material.

(defmacro defgluecode ((&key export ) form)
  `(eval-when (:load-toplevel :execute)
     (add-gtk-glue ',form :export ,export)))

(defun add-gtk-glue (form &key export )
  (push form gtk-gluecode)
  (unless lib-packaging
    (when export (setq export ':gtk)))
  (cond ((atom form)
         (when export (gtk-export form export)))
        ((member (car form) '(defun defmacro))
         (when export
           (gtk-export (if (stringp (second form))
                         (make-instance 'cssym :name (second form))
                         (format nil "~(~A~)" (second form)))
                       export)))
        ((member (car form) '(defvar defconstant defparameter))
         (when export
           (gtk-export (if (stringp (second form))
                         (make-instance 'cssym :name (second form))
                         (format nil "~(~A~)" (second form)))
                       export)))
        (t (when export
             (error "Fixme: don't know how to export ~s."
                    form))))
  (values))

;;;
;;; common output routines

(defun enum-constants (def)
  ;; return lisp constant definitions for all enums in def
  (loop for s in (fourth def)
        collect `(defconstant ,(lisp-name (car s)) ,(cadr s))))

(defun output-prelude (fil)
  (let ((*print-case* ':downcase))
    (gtk-banner fil)
    ;; add our handcoded defs just after package definition
    (write-string prelude-string fil)
    (format fil "~%;;; begin generated output~%")
    (values)))

(defun output-packaging (fil)
  (let ((uses (cssym-list! #+(and openmcl (not cffi-lambda-gtk))
                           (list :use :ccl :common-lisp)
                           #+(and (or cmu sbcl) (not cffi-lambda-gtk))
                           (list :use "#+:sbcl" :sb-alien
                                 "#+:cmu" :alien "#+:cmu" :c-call
                                 :common-lisp)
                           #+(and cffi-lambda-gtk openmcl)
                           (list :use :ccl :common-lisp :cffi)
                           #+(and cffi-lambda-gtk (or cmu sbcl clisp))
                           (list :use :cffi
                                 :common-lisp)))
        (gshadow #+(and opencml (not cffi-lambda-gtk)) nil
                 #+(and (or cmu sbcl) (not cffi-lambda-gtk))
                 (cssym-list! (list "#+:cmu" '(:shadow :callback)))
                 #+cffi-lambda-gtk
                 (cssym-list! (list '(:shadow :callback))))
        (*print-case* ':downcase))
    ;; either make one package and keep prefixes or make separate
    ;; packages and strip prefixes
    (if (not lib-packaging)
      (pprint `(defpackage :gtk (:nicknames :gdk :g :atk :pango)
                           ,uses)
              fil)
      (pprint `(eval-when (:compile-toplevel :load-toplevel :execute)
                 (defpackage :gtk ,uses
                             (:shadow :true :false :fill))
                 (defpackage :gdk ,uses
                             (:shadow :copy :invert :xor :clear :and :or
                                      :set :delete :plus :mouse :destroy
                                      :map :unmap :scroll) )
                 (defpackage :g ,uses ,@ gshadow)
                 (defpackage :atk ,uses)
                 (defpackage :pango ,uses (:shadow :break)))
              fil))
    (format fil "~%~%;; make :gtk the default package.")
    (pprint '(in-package :gtk) fil)
    (values)))

(defun output-gluecode (fil)
  (let ((*print-case* ':downcase))
    (format fil "~%~%;;; GTK glue code~%")
    (dolist (e (reverse gtk-gluecode))
      (pprint (cssym-list! e) fil))))

(defun output-constants (fil)
  (let ((*print-case* ':downcase))
    (format fil "~%~%;;; constants~%")
    (loop for e in gtk-entries
          for d = (ffi-entry-def e)
          when (and (eql (ffi-entry-include e) ':API)
                    (enum-def? d))
          do (dolist (x (cssym-list! (enum-constants d)))
               (gtk-export (second x))
               (pprint x fil)))
    (values)))

(defun output-accessors (fil)
  (format fil "~%~%;;; accessors~%")
  (let ((*print-case* ':downcase)
        ;; turn off prefix stripping to protect slot names and
        ;; accessor from being affected.
        )
    (loop for e in gtk-entries
          for d = (ffi-entry-def e)
          when (AND (eq (car d) 'struct)
                    (eq (ffi-entry-include e) ':API))
          do (let ((pre (lisp-name (def-name d)))
                   (out (cssym-list! (copy-tree
                                       (struct-accessor-defs e)))))
               pre
               (when out
                 (dolist (x out)
                   (gtk-export (second x))
                   (pprint x fil)))))
    (values)))

(defun output-functions (f)
  #+sbcl
  (declare (sb-ext:muffle-conditions style-warning
                                     sb-ext:compiler-note))
  (format f "~%~%;;; functions~%")
  (let ((*print-case* ':downcase))
    (loop for e in gtk-entries
       for d = (ffi-entry-def e)
       when
       (and (function-def? d)
            (eql (ffi-entry-include e) ':api))
       do
         (gtk-export (make-instance 'cssym :name (lisp-name (def-name d)))
                     )
         (pprint (cssym-list! (wrapper-def d))
                 f))
    (values)))

(defun output-postlude (fil)
  (let ((*print-case* ':downcase))
    (terpri fil)
    (format fil postlude-string)
    (format fil "~%(eval-when (:load-toplevel :execute)~%")
    (when g-exports
      (pprint `(export ',(reverse g-exports) :g)
              fil))
    (when gdk-exports
      (pprint `(export ',(reverse gdk-exports) :gdk)
              fil))
    (when atk-exports
      (pprint `(export ',(reverse atk-exports) :atk)
              fil))
    (when pango-exports
      (pprint `(export ',(reverse pango-exports) :pango)
              fil))
    (when gtk-exports
      (pprint `(export ',(reverse gtk-exports) :gtk)
              fil))
    (terpri fil)
    (pprint '(pushnew ':gtk *features*) fil)
    (format fil "~%)~%~%;;; end generated output~%~%")
    (values)))
