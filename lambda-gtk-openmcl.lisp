;;; generates gtk interface file for Openmcl:
;;;   $ cd /path/to/lambda-gtk
;;;   $ openmcl
;;;   ? (load "lambda-gtk-openmcl")
;;;   ? (lambda-gtk "gtkffi-openmcl.lisp")
;;;   ? (compile-file "gtkffi-openmcl")
;;;   ? (load "gtkffi-openmcl")

(in-package :lambda-gtk)

;; (eval-when (:load-toplevel :compile-toplevel :execute)
;;   (load (make-pathname :name "lambda-gtk-common" :type nil
;;                        :defaults *load-pathname*)))

(defparameter alien-type-mappings
  ;; map basic ffi types to OpenMCL foreign types
  ;; (see ccl/lib/foreign-types.lisp)
  `(
    ( (void ()) :void)
    ( (char ()) :char #+darwin ccl:%get-signed-byte
                      #-darwin ccl:%get-unsigned-byte)
    ( (signed-char ()) :signed-char ccl:%get-signed-byte)
    ( (short ()) :short ccl:%get-signed-word) ; 16
    ( (int ())   :int ccl:%get-signed-long)
    ( (long ()) :long ccl:%get-signed-long)
    ( (unsigned-char ()) :unsigned-char ccl:%get-unsigned-byte)
    ( (unsigned-short ()) :unsigned-short ccl:%get-unsigned-word)
    ( (unsigned-int ()) :unsigned-int ccl:%get-unsigned-long)
    ( (unsigned ()) :unsigned-int ccl:%get-unsigned-long)
    ( (unsigned-long ()) :unsigned-long )
    ( (float ()) :float ccl:%get-single-float)
    ( (double ()) :double ccl:%get-double-float)
    ( (long-long ()) :signed-double-word ccl::%%get-signed-longlong)
    ( (unsigned-long-long ()) :unsigned-double-word
                              ccl::%%get-unsigned-longlong)
    ;; handle these types specially
    ( (typedef "gboolean") :boolean ccl:%get-signed-long)
    ( (pointer (char ())) :char* ccl:%get-cstring)
    ( (pointer (typedef "gchar")) :char* ccl:%get-cstring)
    ( (pointer (typedef "guchar")) :char* ccl:%get-cstring)
    ( (pointer (int ())) :int* ccl:%get-signed-long)
    ( (pointer (typedef "gint")) :int*  ccl:%get-signed-long)
    ( (pointer (float ())) :float* ccl:%get-single-float)
    ( (pointer (typedef "gfloat")) :float* ccl:%get-single-float)
    ( (pointer (double ())) :double* ccl:%get-double-float)
    ( (pointer (typedef "gdouble")) :double* ccl:%get-double-float)
    ))

(defun openmcl-type (typ )
  ;; return info for types built on basic openmcl types.
  (labels ((findit (x)
           (let ((y (find x alien-type-mappings
                          :key #'first :test #'equal)))
             (cdr y))))
    (or (findit typ )
        (case (car typ)
          (( typedef )
           (if (stringp (cadr typ))
             (let ((def (get-def (cadr typ))))
               (and def 
                    (if (struct-def? def)
                      '(:struct ccl:%inc-ptr)
                      (openmcl-type (def-data def)))))
             (error "Nonstring typedef: ~s." typ)))
          (( pointer )
           (let ((x (openmcl-type (second typ))))
             (let ((y (find x alien-type-mappings :key #'cdr
                            :test #'eq)))
               ;(print (list :y-> y))
               (if y
                 (or (findit `(pointer ,(first y)))
                     '(:address ccl:%get-ptr))
                 '(:address ccl:%get-ptr)))))
          (( array )
           (let ((x (openmcl-type (third typ))))
             (if x
               (cons ':array x)
               nil)))
          (( enum-ref )
           (findit '(int ())))
          ((struct-ref)
           '(:struct ccl:%inc-ptr))
          ((union-ref )
           `(:union ccl:%inc-ptr))
          (t nil)))))

(defun activate-entries ()
  ;; mark entries in .api file for inclusion in interface file.
  (maphash
   (lambda (key entry)
     (let* ((def (ffi-entry-def entry))
            (nam (def-name def)))
       (case (car def)
         ((function )
          (unless (or (upper-case-name? nam)
                      (internal-name? nam))
            (when (gethash nam gtk-api)
              (setf (ffi-entry-include entry) :api))))
         (( type )
          (when (gethash key gtk-api)
            (when (enum-ref-def? def)
              (let ((enum (get-entry (second (fourth def)))))
                (setf (ffi-entry-include enum) ':api)))))
         ((struct union)
          (let ((x (gethash key gtk-api))
                (i ()))
            (when x
              (let ((y (assoc ':accessors (cddr x))))
                ;; activate if :accessors unspecified or if slots are
                ;; actually listed
                (when (or (not y) (cdr y))
                  (setf (ffi-entry-include entry) ':api)
                  ;; iterate slots and cache any that are structs.
                  ;; this info is used by make-accessor-def
                  (mapslots def
                            (lambda (name type bf?) 
                              bf?
                              (let ((d (struct-slot? name type)))
                                (if d (push (list name d) i)))))
                  (when i
                   (push (cons ':included i)
                         (ffi-entry-data entry)))))))))))
   gtk-ffi))

;;;
;;; functions ....

(defun function-arg-type (x) (openmcl-type x))

; (function-arg-type '(pointer (typedef "GdkModifierType")))
; (function-arg-type '(typedef "gdouble"))
; (function-arg-type '(typedef "gboolean"))
; (function-arg-type '(pointer (typedef "gint")))
; (function-arg-type '(pointer (typedef "GList")))
; (function-arg-type '(pointer (int ())))
; (function-arg-type '(pointer (typedef "gint")))
; (function-arg-type '(pointer (typedef "gdouble")))
; (function-arg-type '(typedef "gint"))
; (function-arg-type '(pointer (char ())))
; (function-arg-type '(pointer (typedef "gchar")))
; (function-arg-type '(char ()))
; (function-arg-type '(typedef "gpointer"))
; (function-arg-type '(pointer (pointer (typedef "PangoAttrList"))))
; (function-arg-type '(pointer (pointer (char ()))))
; (function-arg-type '(pointer (typedef "GdkAtom")))

(defun function-def-args (args)
  ;; gather all args until the first 'void' arg
  (let ((argnames '(a b c d e f g h i j k l m n o p q r s u v w x y)))
    (loop for a in args
          for p = (or (pop argnames)
                      (error "Fixme: not enuff argnames"))
          for x = (openmcl-type a)
          until (eql (car x) ':void)
          collect (cons p x))))

(defun pass-value (arg &optional acc?)
  ;; insure basic types are coerced to expected values.
  (let ((typ (second arg)))
    (cond ((eql typ ':boolean)
           (gtk-boolean (first arg)))
          ((find typ '(:double :gdouble :double*))
           (gtk-double (first arg)))
          ((find typ '(:float :gfloat :float*))
           (gtk-single (first arg)))
          ((and acc? (eql typ ':char*))
           `(ccl::make-cstring , (first arg)))
          (t
           (first arg)))))

(defun return-value (arg)
  ;; arg is: (var type accessor)
  (case (second arg)
    ((:boolean)
     `(if (= 1 ,(first arg)) t nil)) ; 1=+gtk-true+
    ((:char* :int* :float* :double*)
     (list (third arg) (first arg)))
    (t arg)))

;;;
;;; wrapper-def returns api wrapper function

(defun wrapper-def (def)
  (let* ((cname (third def))
         (lname (lisp-name cname))
         (args (function-def-args (second (fourth def))))
         (pars (loop for a in args collect (first a)))
         (retn (cons 'z (function-arg-type (third (fourth def)))))
         (cstrs '())  ; with-cstr bindings
         (frecs '())  ; rlet bindings
         (fcall '())  ; vals passed in trap call
         (rvals '())  ; return value(s) from ffi call
         (forms '())
         (recvs '(r1 r2 r3 r4 r5 r6 r7 r8 r9 r10))
         (strvs '(s1 s2 s3 s4 s5 s6 s7 s8 s9 s10)))
    (dolist (a args)
      ;; a = (param type accessor)
      (case (second a)
        ((:float :double :boolean)
         (push (pass-value a) fcall))
        ((:char*)
         (let ((str (list (or (pop strvs)
                              (error "Fix me: not enuff str vars."))
                          (first a))))
           (push str cstrs)
           (push (first str) fcall)))
        ((:int* :float* :double*)
         (let ((rec (list (or (pop recvs)
                              (error "Fix me: not enuff rec vars."))
                          (getf '(:int* :int :float* :float
                                  :double* :double)
                                (second a))
                          (pass-value a))))
           ;; add to rlet bindings
           (push rec frecs)
           (push (first rec) fcall)
           ;; subst in the cvariable
           (push (return-value (cons (first rec) (cdr a))) 
                 rvals)))
        (t (push (pass-value a) fcall))))
    (setq cstrs (nreverse cstrs))
    (setq frecs (nreverse frecs))
    (setq rvals (nreverse rvals))
    (setq fcall (nreverse fcall))
    (setq fcall (cons (format nil "#_~A" cname) fcall))
    ;; retn is argtype of return value. possible wrap its value
    ;; and add it to any pointer return values.
    (let ((chk (return-value retn)))
      (cond ((not (eql chk (return-value retn)))
             (push chk rvals))
            ((not (null rvals))
             (push (first retn) rvals))))
    (setq forms 
          (if rvals
            `(let ((, (first retn) , fcall))
              (values ,@ (loop for e in rvals collect e)))
            fcall))
    (when frecs (setf forms `(ccl:rlet (,@frecs) ,forms)))
    (when cstrs (setf forms `(ccl:with-cstrs (,@cstrs) ,forms)))
    `(defun ,lname ,pars ,forms)))

; (pprint (wrapper-def (get-def "gtk_init")))
; (pprint (wrapper-def (get-def "g_signal_connect_data")))
; (pprint (wrapper-def (get-def "pango_layout_get_pixel_size")))
; (pprint (wrapper-def (get-def "gdk_window_get_pointer")))
; (pprint (wrapper-def (get-def "pango_glyph_string_index_to_x")))
; (pprint (wrapper-def (get-def "pango_glyph_string_x_to_index")))
; (pprint (wrapper-def (get-def "gdk_string_extents")))
; (pprint (wrapper-def (get-def "gdk_window_get_geometry")))
; (pprint (wrapper-def (get-def "gtk_ruler_get_range")))
; (pprint (wrapper-def (get-def "pango_language_matches")))
; (pprint (wrapper-def (get-def "gtk_menu_get_tearoff_state")))

;;;
;;; structs...

(defun ffi-case (name)
  ;; respell typename as OpenMCL type symbol
  (let* ((len (length name)))
    (loop with m = :down and l = ()
          for i below len
          for c = (elt name i)
          do (cond ((upper-case-p c)
                    (if (eql m :down)
                      (progn (setq m :up)
                             (push #\< l))))
                   (t (if (eql m :up)
                        (progn (push #\> l)
                               (setq m ':down)))))
          (push c l)
          finally
          (progn (if (eql m :up)
                   (push #\> l))
                 (return (coerce (nreverse l) 'string))))))

(defun struct-slot-accessor (struct arry? slot &optional slot2)
  ;; if arry? is true its the dimension of the array in slot
  ;; slot = (name type wid)
  (let* ((nam (if slot2 
                (struct-accessor-name struct (first slot) (first slot2))
                (struct-accessor-name struct (first slot))))
         ;; Make an openmcl slot reference from name, first
         ;; substitiute _ for - back into slot string.
         (acc (ffi-case (concatenate 'string ":"
                                     (substitute #\_ #\- nam))))
         (isa (function-arg-type (if slot2 (second slot2)
                                     (second slot))))
         ;; wrap optional value like other funtions in api.
         (val (pass-value (cons 'val isa) t)))

    (if (not arry?)
      `(defun ,nam (ptr &optional (val nil vp))
         (if vp 
           (progn (setf (pref ptr ,acc) ,val)
                  val)
           (pref ptr ,acc)))
      (let* ((wid (/ (third slot) arry?))
             (max arry?)
             (get `(,(car (last isa))
                     (ccl:pref ptr ,acc)
                     (* index ,wid)))
             (err (format nil "\"~a[~~d]: index larger than ~d.\""
                          acc max)))
        (if (eql (second isa) :struct)
           (error "Fix me: don't know how to access arrays of structs.")
          `(defun ,nam (ptr index &optional ,val)
             (unless (< index ,max) (error ,err index))
             (if ,val (progn (setf ,get ,val) val) , get)))))))

; (pprint (cssym-list! (struct-accessor-defs (get-entry "GdkColor"))))
; (cssym-list! (struct-accessor-defs (get-entry "GtkGammaCurve")))
; (pprint (cssym-list! (struct-accessor-defs (get-entry "GtkAdjustment"))))
; (pprint (cssym-list! (struct-accessor-defs (get-entry "GtkWidget"))))
; (pprint (cssym-list! (struct-accessor-defs (get-entry "GtkStyle"))))

;;;
;;;
;;;

(defparameter prelude-string "
(in-package :lambda-gtk)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (export '(*gtk-libdir* *gtk-libfiles*) :lambda-gtk)
  (defvar *gtk-libdir* \"/sw/lib/\")
  (defvar *gtk-libfiles*  '(\"libgtk-x11-2.*.dylib\"))
  (flet ((libpath (lib &aux p m)
           (setq p (concatenate 'string *gtk-libdir* lib))
           ;; sigh, apparently Fink's lib numbers can vary...
           (setq m  (directory p))
           (cond ((null m)
                  (error \"Library ~S not found. Either GTK is not installed or else lambda-gtk:*gtk-libdir* needs to be set to the directory containing GTK on your machine.\" p))
                 ((cdr m)
                  (ccl::native-translated-namestring
                   (first (sort #'string-greaterp m
                                           :key #'string))))
                 (t
                  (ccl::native-translated-namestring (car m))))))
    (dolist (l *gtk-libfiles*)
      (open-shared-library (libpath l)))))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (if (probe-file \"ccl:darwin-headers;gtk2;\" )
    (use-interface-dir :gtk2)
    (error \"Interface directory ccl:darwin-headers;gtk2; does not exist.\")))
")

;;;
;;; define the gluecode. strings are replaced by cssyms.
;;;

(defgluecode (:export :gtk) (defconstant "+gtk-false+" 0))
(defgluecode (:export :gtk) (defconstant "+gtk-true+" 1))
(defgluecode (:export :g) (defun "g-nullptr" () (%null-ptr)))
(defgluecode (:export :g) (defun "g-nullptr?" (x) (%null-ptr-p x)))
(defgluecode (:export nil) (defvar *gtk-init* nil))
(defgluecode (:export :gtk)
  (defun "gtk-init-ensure" (&optional strings)
    (declare (ignore strings))
    (unless *gtk-init*
      ("gtk-init" 0 ("g-nullptr"))
      (setq *gtk-init* t))
    *gtk-init*)  )
(defgluecode (:export :gtk)
  (defmacro struct-alloc (&rest args)
    `(ccl::make-record ,@args)))
(defgluecode (:export :gtk)
  (defun struct-free (x) ("#_free" x)))
(defgluecode (:export :gtk)
  (defun string->cstring (str) (ccl::make-cstring str)))
(defgluecode (:export :gtk)
  (defun cstring->string (cstr) (ccl:%get-cstring cstr)))
(defgluecode (:export :gtk)
  (defun cstring-alloc (str) (ccl::make-cstring str)))
(defgluecode (:export :gtk)
  (defun cstring-free (cstr) (ccl::free cstr) (values)))
(defgluecode (:export :gtk)
  (defmacro define-signal-handler (name return params &body body)
    `(ccl:defcallback ,name ,(nconc (loop for p in params
                                          append
                                          (if (consp p) (reverse p)
                                              (list :address p)))
                                    (list return))
       ,@body)))
(defgluecode (:export :g) (defun "g-callback" (x) x))
(defgluecode (:export :g)
  (defun "g-signal-connect" (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                             ("g-nullptr") 0)))
(defgluecode (:export :g)
  (defun "g-signal-connect-after"
      (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                         ("g-nullptr") 1)))
(defgluecode (:export :g)
  (defun "g-signal-connect-swapped"
      (instance detailed_signal c_handler data)
    ("g-signal-connect-data" instance detailed_signal c_handler data 
                           ("g-nullptr") 2)))

(defun lambda-gtk (p &optional (lib-packaging t))
  (declare (ignore gtk-libdir))
  (let ((*print-case* ':downcase))
    (gtk-ffi-init)
    (format t "~%loading api...")
    (mapdefs #'doapi (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                    :name "gtk.api"))
    (format t "~%loading ffi...")
    (mapdefs #'dodef (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                    :name "gtk.ffi"))
    (format t "~%generating entries...")
    (activate-entries)
    (progn (setf gtk-entries (gtk-entries)) t)
    (when p
      (format t "~%writing output in ~s..." p)
      (outfile (f p) 
               (output-prelude f)
               (output-packaging f)
               (output-gluecode f)
               (output-constants f)
               (output-accessors f)
               (output-functions f)
               (output-postlude f)))
    (format t "~%Done.")
    t))

