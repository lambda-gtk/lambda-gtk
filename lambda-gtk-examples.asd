;;; -*- Lisp -*-

(in-package #:cl-user)

(defpackage #:lambda-gtk-examples-system
  (:use #:cl
        #:asdf))

(in-package #:lambda-gtk-examples-system)

(defsystem #:lambda-gtk-examples
  :version "0.1"
  :components
  ((:module :examples
            :components ((:file "packages")
                         (:file "examples" :depends-on ("packages")))))
  :depends-on (#:lambda-gtk))
