(in-package :lambda-gtk)

;;;
;;; entries from the .ffi file are shifted to specific sections in the
;;; output file:

(defconstant class-constant 10000) ; enum constants
(defconstant class-ctype    20000) ; refer to int, char etc.
(defconstant class-pointer  30000) ; pointer types
(defconstant class-basic    40000) ; refer to c-type or pointers
(defconstant class-struct1  50000) ; refer to ctypes pointers or basic
(defconstant class-struct2  60000) ; refer to other structs
(defconstant class-accessor 70000) ; struct slot accessors
(defconstant class-function 90000) ; function entry
(defconstant class-macro    100000) ; macro entry

(defun class-unknown? (e)
  (let ((n (ffi-entry-number e)))
    (< n class-constant)))
(defun class-constant? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-constant n) (< n class-ctype)
         class-constant)))
(defun class-ctype? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-ctype n) (< n class-pointer)
         class-ctype)))
(defun class-pointer? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-pointer n) (< n class-basic))))
(defun class-basic? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-basic n) (< n class-struct1)
         class-basic)))
(defun class-struct1? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct1 n) (< n class-struct2)
         class-struct1)))
(defun class-struct2? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct2 n) (< n class-accessor)
         class-struct2)))
(defun class-struct? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct1 n) (< n class-accessor))))
(defun class-accessor? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-accessor n) (< n class-function))))
(defun class-function? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-function n) (< n class-macro)
         class-function)))
(defun class-macro? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-macro n) class-macro)))

(defparameter ctype-mappings
  ;; map basic ffi types to cffis foreign types
  `(
    ( (char ()) :char)
    ( (signed-char ()) :char)
    ( (short ()) :short)
    ( (int ())   :int)
    ( (long ()) :long)
    ( (unsigned-char ()) :unsigned-char)
    ( (unsigned-short ()) :unsigned-short)
    ( (unsigned-int ()) :unsigned-int)
    ( (unsigned ()) :unsigned-int)
    ( (unsigned-long ()) :unsigned-long)
    ( (float ()) :float)
    ( (double ()) :double)
    #-cffi/no-long-long
    ( (long-long ())  :long-long)
    #+cffi/no-long-long
    ( (long-long ())  :long)
    #-cffi/no-long-long
    ( (unsigned-long-long ()) :unsigned-long-long)
    #+cffi/no-long-long
    ( (unsigned-long-long ())  :unsigned-long)
    ( (void ()) :void)

    ( (pointer (void ()))  :pointer)
    ( (pointer (char ()))  :string  )
    ( (pointer (gchar ()))  :string  )
    ( (pointer (guchar ()))  :string  )

    ))

(defun cffi-type (x &optional *t)
  ;; return an cffi alien type specifier for a ffi type decl
  ;; if *t is t then force (pointer ...) to (* t)
  (cond ((null x)
         nil)
        ((ctype? x)
         (ctype? x))
        ((eql (car x) 'pointer)
         :pointer)
        ((eql (car x) 'typedef)
         (cadr x))
        ((member (car x) '(struct-ref union-ref))
         (second x))
        ((eql (car x) 'function)
         :pointer)
        ((eql (car x) 'array)
         ;; dont wrap (struct ...) around typdefs
         (if (eql (car (third x)) 'typedef)
           (list (second (third x)) :count (second x))
           (let ((type (cffi-type (third x) *t)))
             (if (consp type)
                 (list (first type) :count (* (third type) (second x)))
                 (list (cffi-type (third x) *t) :count (second x))))))
        ((eql (car x) 'enum-ref) :int)
        (t (ctype? x))))

;; (cffi-type '(unsigned-long nil)) => :unsigned-long
;; (cffi-type '(char nil)) => :char
;; (cffi-type '(pointer (char nil))) => :char
;; (cffi-type '(enum-ref 154_/sw/include/gtk-2.0/gtk/gtk.h)) => :int
;; (cffi-type '(void nil)) => :void
;; (cffi-type '(typedef "GtkStyle")) => "GtkStyle"
;; (cffi-type '(typedef __gnuc_va_list)) => __gnuc_va_list
;; (cffi-type '(pointer (typedef GList))) => :pointer
;; (cffi-type '(array 31 (typedef GdkColor))) => (:GdkColor :count 31)
;; (cffi-type '(array 5 (array 10 (typedef Gint)))) => (Gint :count 50)
;; (cffi-type '(array 31 (int nil))) => (:int :count 31)
;; (cffi-type '(function nil (void nil))) => :pointer
;; (cffi-type '(union-ref 4549_/sw/include/gtk-2.0/gtk/gtk.h)) => 4549_/sw/include/gtk-2.0/gtk/gtk.h
;; (cffi-type '(struct-ref _GtkTextLayout)) => _GtkTextLayout
;;

(defun atomic-def? (def &optional grovel?)
  ;; def is "atomic" if it does not reference any structs.  atomics
  ;; appear in the output file before any struct definitions. if
  ;; grovel? is true then def is a stuct slot's type expresssion.  in
  ;; this case we descend into the type list and check all refs.
  (labels ((checkfail (x)
             (cond ((consp x)
                    (or (checkfail (car x))
                        (checkfail (cdr x))))
                   ((stringp x)
                    (let ((d (get-def x)))
                      (if (not d) t
                          (if (atomic-def? d) nil t))
                      ))
                   (t nil))))
    (if grovel?
      (if (checkfail def) nil t)
      (case (car def)
        (type
         (let ((x (fourth def)))
           (case (car x)
             (( struct-ref union-ref ) nil)
             (( struct union ) nil)
             (( enum-ref ) t)
             (t 
              ;; grovel through function and pointer type specs
              ;; and check embedded typedefs...
              (if (checkfail x) nil t)))))
        (( function macro )  nil )
        ((struct union) nil)
        ((enum-ref ) t)
        (t t)))))

(defun atomic-struct? (def)
  ;; return t if struct's slots only references atomic types or
  ;; pointers (which are translated as (* t) in the output file)
  (let ((slots (fourth def)))
    (loop for slot in slots
          for type = (second (second slot))
          always (or (eql (car type) 'pointer)
                     (atomic-def? (second (second slot)) t)))))

;;;
;;; api and ffi loading routines
;;;

(defun classify-entries ()
  ;; classify entry types so that we can resolve type dependancies
  ;; in the output FFI file
  (maphash (lambda (k e)
             k
             (when (ffi-entry-include e)
               ;; skip if already classified...
               (unless (> (ffi-entry-number e) class-constant)
                 (let ((d (ffi-entry-def e)))
                   (case (car d)
                     (( type )
                      (cond ((struct-type-def? d)
                             ;;GtkAllocation=GdkRectangle
                             (incf (ffi-entry-number e) class-struct2))
                            ((pointer-def? d)
                             (incf (ffi-entry-number e) class-pointer))
                            ((enum-ref-def? d)
                             (incf (ffi-entry-number e) class-ctype))
                            ((ctype? (fourth d))
                             (incf (ffi-entry-number e) class-ctype))
                            ((atomic-def? d)
                             (incf (ffi-entry-number e) class-basic))
                            ((eql (car (fourth d)) 'typedef)
                             ;; (type foo (typedef bar))
                             (let* ((s (gethash (cadr (fourth d)) 
                                                gtk-ffi))
                                    (n (and s (or (class-struct1? s)
                                                  (class-struct2? s)))))
                               (if n
                                 (incf (ffi-entry-number e)
                                       n)
                                 (warn "unresolved typedef class: ~S" e))))
                            (t
                             (warn "Failed to classify: ~s" d))))
                     (( struct union )
                      (if (atomic-struct? d)
                        (incf (ffi-entry-number e) class-struct1)
                        (incf (ffi-entry-number e) class-struct2)))
                     (( enum )
                      (incf (ffi-entry-number e) class-constant))
                     (( function )
                      (incf (ffi-entry-number e) class-function))
                     (( macro )
                      (incf (ffi-entry-number e) class-macro))
                     (t (format t "~%Failed to classify: ~s" d)))))))
           gtk-ffi))

(defun activate-struct-slots (entry)
  (let ((def (ffi-entry-def entry))
        (bitfields (list))
        (include (list )))
    ;; check for bitfield slots
    (when (eql (ffi-entry-include entry) ':api)
      (let ((top nil)
            (bfs nil))
        (mapslots
         def
         (lambda (name type bf)
           (if bf 
             (progn
               (when (not bfs)
                 (setq top (list (second type) 0)
                       bfs (list top)))
               (push name bfs)
               (incf (second top) (second bf))
               ;;(unless (< (second top) 32)
               ;;  (warn "~A.~A: BITFIELD WITDH ~S WIDER THAN 32 BITS."
               ;;        (def-name def) name (second top)))
               )
             (if bfs
               (progn (push (nreverse bfs) bitfields)
                      (setq bfs nil top nil))))))
      ;; end slot may be bitfield...
      (when bfs (push (nreverse bfs) bitfields))
      (when bitfields
        (push (cons :bitfields (reverse  bitfields))
              (ffi-entry-data entry)))))

    ;; check for included slots
    (mapslots def
              (lambda (name type bitfield?)
                (declare (ignore bitfield?))
                (let ((d (struct-slot? name type)))
                  (if d (push (list name d) include)))))
    ;; include is a list of (slot def) where each def is a structure
    ;; that slot inlines.
    (when include
      (loop for i in include
         for d = (second i)
         for n = (def-name d)
         for e = (get-entry n)
         do
         (progn (unless (ffi-entry-include e)
                  ;; include if ffi but no API accessors.
                  (setf (ffi-entry-include e) T)
                  ;; check slots recursively
                  (activate-struct-slots e)
                  )))
      (setq include (reverse include))
      ;; list of inlined entries: ("slotname" "structname")
      ;; CAREFUL: the inlined struct may be in an array!
      (push (cons :included include)
            (ffi-entry-data entry)))))

; (activate-struct-slots (get-def "_GtkWidget"))

(defun activate-entries ()
  ;; map over the ffi entries and mark only API entries for inclusion
  ;; in the output file. This recurses on slot types for every
  ;; included struct.  Entries included from the API are marked by the
  ;; symbol ':api, entries included from recursive slot inspection are
  ;; marked by 't.
  (maphash
   (lambda (key entry)
     (let* ((def (ffi-entry-def entry))
            (nam (def-name def)))
       (case (car def)
         (( function )                  ; macro
          (unless (or (upper-case-name? nam)
                      (internal-name? nam))
            (when (gethash nam gtk-api)
              (setf (ffi-entry-include entry) :api))))
         (( struct union )
          (let ((e (gethash key gtk-api)))
            (when e
              (setf (ffi-entry-include entry) :api)
              (activate-struct-slots entry))))
         (( type )
          (cond ((gethash key gtk-api)
                 (setf (ffi-entry-include entry) :api)
                 ;; if its an enum typedef activate the enum itself...
                 (when (enum-ref-def? def)
                   (let ((enum (get-entry (second (fourth def)))))
                     (setf (ffi-entry-include enum) ':api))))
                ((pointer-def? def) 
                 (setf (ffi-entry-include entry) t))
                ((atomic-def? def)
                 (setf (ffi-entry-include entry) t))
                ;; type is typedef of a type (ie GdkAllocation)
                ((and (eql (car (fourth def)) 'typedef)
                      (ffi-entry-include 
                       (get-entry (second (fourth def)))))
                 (setf (ffi-entry-include def) t))
                (t nil)))
         ;;(( enum )
         ;; (dolist (d (fourth def))
         ;;   (when (eql (car (gethash (car d) gtk-api)) ':constant)
         ;;     (setf (ffi-entry-include entry) :api)
         ;;     (return))))
         )))
   gtk-ffi))

#+nil
(defun topological-sort (list &key (key #'car) (depends-on #'cdr))
  (let ((nodes (make-hash-table :test 'equal)) (sorted-list '()))
    (dolist (item list)
      (setf (gethash (funcall key item) nodes)
            (list 'not-yet-visited item)))
    (labels ((dfs-visit (node)
               (case (car node)
                 (not-yet-visited
                   (setf (car node) 'visiting)
                   (dolist (child (funcall depends-on (second node)))
                     (dfs-visit (gethash child nodes)))
                   (setf (car node) 'visited)
                   (push node sorted-list))
                 (visiting
                   (error "~&Detected cycle containing ~A" (second node))))))
      (loop for v being the hash-values of nodes
           do (dfs-visit v)))
    (setf sorted-list (nreverse sorted-list))
    (map-into sorted-list #'second sorted-list)))

(defun topological-sort (list &key (key #'car) (depends-on #'cdr))
  (let* ((nodes (make-hash-table :test 'equal)) (sorted-list nil))
    (dolist (item list)
      (setf (gethash (funcall key item) nodes)
            (list 'white item)))
    (labels ((get-dependencies (node)
               (loop for node in (funcall depends-on (second node))
                  for id = (funcall key node)
                  when (gethash id nodes)
                  collect (gethash id nodes)))
             (dfs-visit (node)
               (setf (car node) 'gray)
               (dolist (child (get-dependencies node))
                 (cond ((eq (car child) 'white)
                        (dfs-visit child))
                       ((eq (car child) 'gray)
                        (format t "~&Detected cycle containing ~A" (second child)))))
               (setf (car node) 'black)
               (push node sorted-list)))
      (maphash (lambda (k v)
                 (declare (ignore k))
                 (when (eq (car v) 'white)
                   (dfs-visit v)))
               nodes)
      (let ((result (mapcar (lambda (node)
                             (second node))
                     (nreverse sorted-list))))
        result))))

(defun sort-struct2s ( )
  (let ((struct2s (list))
        (entrynum (+ class-struct2 1)))
    ;; collect all the struct2s
    (maphash (lambda (k v)
               k
               (when (class-struct2? v)
                 (push v struct2s)))
             gtk-ffi)
    ;; sort structs so includers are later than includees
    (setf struct2s
          (topological-sort struct2s
                            :key (lambda (x)
                                   (car x))
                            :depends-on
                            (lambda (x)
                              (let (dependencies
                                    (def (ffi-entry-def x)))
                                (case (car def)
                                  (struct
                                   (mapslots (ffi-entry-def x)
                                             (lambda (name type bitfield?)
                                               (declare (ignore bitfield?))
                                               (let ((d (struct-slot? name type)))
                                                 (when d (push (list (def-name d)) dependencies)))))
                                   (when (member '("GdkRectangle") dependencies :test #'equal)
                                     (push '("GtkAllocation") dependencies)))
                                  (type (push '("GtkRectangle") dependencies))
                                  (union (setf dependencies (mapcar (lambda (v)
                                                                  (list (def-name (second v))))
                                                                (cdr (assoc ':included
                                                                            (ffi-entry-data x)))))))
                                dependencies))))
    ;; set ffi entry order to sorted order.
    (dolist (s struct2s)
      (setf (ffi-entry-number s) entrynum)
      (incf entrynum))
    t))

;;;
;;; definition routines
;;;

(defun sbcl-struct-def (entry)
  ;; add "s" or "u" to unnamed structs
  (let* ((def (ffi-entry-def entry))
         ;; add "s" or "u" to struct/union numbers
         (name (third def)
           #+nil
           (if (digit-char-p (elt (third def) 0))
                 (format nil "~:[u~;s~]~A" (eq (car def) 'struct)
                         (third def))
                 (third def)))
         (bfnum 0)
         (slots (list)))
    (mapslots def
              (lambda (slot type bf)
                (if bf
                  ;; output a bitfield padding slot for the
                  ;; first field in each bitfield.
                  (if (eql (first bf) 0)
                    (push (list (format nil "bf_padding~D"
                                        (incf bfnum))
                                (second type))
                          slots)
                    nil) ;else skip bitfield slot
                  ;; rewrite pointer types to (* t). if type is array
                  ;; then do the same to the array's type
                  (push
                   (let ((typ (cffi-type type)))
                     (if (consp typ)
                         (append (list slot) typ)
                         (list slot typ))
                     #+nil
                     (let ((typ (cffi-type type)))
                       (if (and (consp typ))
                           (list '* t)
                           (if (and (consp typ) (eql (first typ) 'array)
                                  (consp (second typ))
                                  (eql (first (second typ)) '*))
                               (progn (setf (second typ) (list '* t))
                                      typ)
                               typ))))
                   slots))
                ))
    (if (eq (car def) 'struct)
        `(cffi:defcstruct ,name
           ,@(nreverse slots))
        `(cffi:defcunion ,name
           ,@(nreverse slots)))))

(defun cffi-type-def (entry)
  (let* ((def (ffi-entry-def entry))
         (res (if (pointer-def? def)
                  :pointer
                  (cffi-type (fourth def)))))
    ;; (define-alien-type foo void) is error
;;     (when (eql res :void)
;;       (setf res '(struct nil)))
    `(defalien-type , (third def) ,res)))

(defun function-arg-type (arg)
  (declare (optimize (debug 3)))
  ;; retuns a list ({type} [:in-out])
  (let ((isa (cffi-type arg)))
    (cond
      ((eql isa :pointer)
       (let ((x (cffi-type (cadr arg))))
         (cond ((keywordp x)
                (list :pointer x :in-out))
               ((stringp x)
                (let ((e (get-entry x)))
                  (if (and e
                         (class-ctype? e)
                         (not (equal x :string))
                         (not (equal x :pointer)))
                      (list :pointer x :in-out)
                      (list :pointer))))
               (t (list :pointer)))))
      ((consp isa)
       isa)
      (t (list isa)))))

; (function-arg-type '(POINTER (TYPEDEF "GList")))   (:pointer)
; (function-arg-type '(pointer (typedef "gint")))    (:pointer)
; (function-arg-type '(pointer (typedef "GList")))   (:pointer)
; (function-arg-type '(pointer (int ())))            (:pointer :int :in-out) ?
; (function-arg-type '(pointer (typedef "gint")))    (:pointer "gint" :in-out) ?
; (function-arg-type '(pointer (typedef "gdouble"))) (:pointer)
; (function-arg-type '(typedef "gint"))              ("gint" )
; (function-arg-type '(pointer (char ())))           (:string)
; (function-arg-type '(char ()))                     (:char)
; (function-arg-type '(typedef "gpointer"))          ("gpointer")
; (function-arg-type '(pointer (pointer (typedef "PangoAttrList"))))   (:pointer)
; (function-arg-type '(pointer (pointer (char ()))))    (:pointer)
; (function-arg-type '(pointer (typedef "GdkAtom")))    (:pointer)
; (function-arg-type '(array 31 (int nil)))               (:int :count 31)

(defun function-def-args (args)
  ;; gather all args until the first 'void' arg
  (loop for a in args
        for p in '(a b c d e f g h i j k l m n o p q r s u v w x y z)
        for  x = (function-arg-type a)
        until (eql (car x) :void)
        collect (cons p x)))

(defun pass-value (arg)
  ;; wrap basic arg types in expressions to coerce lisp values to that
  ;; type
  (let ((typ (second arg)))
    (cond ((equal typ "gboolean")
           (gtk-boolean (first arg)))
          ((find typ '("double" "gdouble") :test #'equal)
           (gtk-double  (first arg)))
          ((find typ '("float" "gfloat") :test #'equal)
           (gtk-single (first arg)))
          (t
           (first arg)))))

(defun return-value (call type args)
  (if (equal type "gboolean")
    (if (find ':in-out args :test #'member)
      (let ((var (gentemp "V")))
        `(let ((,var (multiple-value-list ,call)))
          (apply #'values (if (= 1 (car ,var)) t nil) ;+gtk-true+
           (cdr ,var))))
      (let ((var (gentemp "V")))
        `(let ((,var ,call))
          (if (= ,var 1) t nil)))) ; +gtk-true+
    call))

(defun wrapper-def (def)
  (let* ((cname (third def))
         (lname (lisp-name cname))
         (args (function-def-args (second (fourth def))))
         (pars (loop for p in args collect (car p)))
         (retn (first (function-arg-type (third (fourth def)))))
         (values (loop for a in args
                    when (< (length a) 4)
                    collect (list (first a))
                    when (and (> (length a) 3)
                            (eq (fourth a) :in-out))
                    collect (list (gentemp)
                                  (let ((type (third a)))
                                    (cond ((stringp type)
                                           `(quote ,(intern (string-upcase type))))
                                          (t type))))))
         (foreign-args (loop for a in values
                          when (/= (length a) 1)
                          collect a))
         (call (list* (format nil "|~A|" cname)
                      (loop for a in args collect (pass-value a))
                      ))
         (foreign-call (list* (format nil "|~A|" cname)
                              (loop for a in values collect (pass-value a))
                              )))
    `(progn
       (defalien ,(format nil "~S" cname) , retn ,@args)
       ,(if foreign-args
            `(defun ,lname ,pars
               (with-foreign-objects ,foreign-args
                 ,@(loop for item in values
                    for arg in args
                    when (/= (length item) 1)
                    collect `(setf (mem-ref ,@item) ,(car arg)))
                 ,(return-value foreign-call retn args)
                 ,(append '(values)
                          (loop for item in values
                             when (/= (length item) 1)
                             collect `(mem-ref ,@item)
                             when (= (length item) 1)
                             collect (car item)))))
            `(defun ,lname ,pars ,(return-value call retn args))))))

#+nil
(defun wrapper-def (def)
  (let* ((cname (third def))
         (lname (lisp-name cname))
         (args (function-def-args (second (fourth def))))
         (pars (loop for p in args collect (car p)))
         (retn (first (function-arg-type (third (fourth def)))))
         (call (list* (format nil "|~A|" cname)
                      (loop for a in args collect (pass-value a))
                      )))
    `(progn
      (defalien ,(format nil "~S" cname) , retn ,@args)
      (defun ,lname ,pars , (return-value call retn args)))))

;;;
;;; accessors
;;;

(defun struct-slot-accessor (struct arry? slot &optional slot2)
  ;; if arry? is true its the dimension of the array in slot
  ;; slot = (name type)
  (let* ((nam (if slot2
                (struct-accessor-name struct (first slot) (first slot2))
                (struct-accessor-name struct (first slot))))
         (exp `(cffi:foreign-slot-value ptr ',struct ',(first slot)))
         ;; wrap the user's value just like regular funtions in api.
         (val
          (pass-value
           (cons 'val
                 (function-arg-type
                  (if slot2 (second slot2)
                      (second slot)))))))
    ;; if its an arry wrap the deref around slot access
    (if arry? (setq exp `(cffi:mem-aref ,exp ',(cffi-type (second slot)) index)))
    ;; wrap slot value with included slot's access
    (if slot2 (setq exp `(cffi:foreign-slot-value ,exp ',(cffi-type (second slot)) ',(first slot2))))
    `(defun ,nam (ptr ,@ (if arry? '(index) nil)
                  &optional (val nil vp))
       (if vp
           (setf ,exp ,val)
           ,exp)
       #+nil
      (let ((,var ,cst))
        (declare (type (alien ,typ) ,var))
        ;; add bounds checking if array
        ,@ (if arry? 
             `((unless (< index ,arry?)
                 (error ,(format nil
                                 "\"struct array index ~~D more than ~D.\""
                                 arry?) index)))
             (list))
        (if vp
          (setf ,exp , val)
          ,exp)))))

; (cssym-list! (struct-accessor-defs (get-entry "GdkColor")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkGammaCurve")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkAdjustment")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkWidget")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkStyle")))

;; handcoded entries and glue code

(defparameter prelude-string "
(in-package :lambda-gtk)
")

;;;
;;; define the gluecode. strings are replaced by cssyms.

;; (defgluecode (:export nil)
;;   (declaim "#+:cmu"  (optimize ("extensions:inhibit-warnings" 3))
;;            "#+:sbcl" ("sb-ext:muffle-conditions" style-warning 
;;                                                 "sb-ext:compiler-note")))

(defgluecode (:export :gtk) (defconstant "+gtk-false+" 0))

(defgluecode (:export :gtk) (defconstant "+gtk-true+" 1))
(defgluecode (:export :g)
  (defun "g-nullptr" () (cffi:null-pointer)))
(defgluecode (:export :g)
  (defun "g-nullptr?" (p) (cffi:null-pointer-p p)))
(defgluecode (:export nil) (defvar *gtk-init* nil))
(defgluecode (:export :gtk)
  (defun "gtk-init-ensure" (&optional strings)
    (declare (ignore strings))
    (unless *gtk-init*
      ("gtk-init" 0 ("g-nullptr"))
      (setq *gtk-init* t))
    *gtk-init*)  )
(defgluecode (:export nil)
  (defmacro defalien (name return &rest args)
    (let* ((l (if (consp name) name (list name (intern name)))) (s (second l)))
      `(progn (declaim (inline ,s)) (cffi:defcfun ,l ,return ,@args)))))
(defgluecode (:export nil)
  (defmacro defalien-type (name type)
    `(cffi:defctype ,name ,type)))
(defgluecode (:export :gtk)
  (defmacro struct-alloc (type &rest inits)
    (let ((inst (gensym)))
      `(let ((,inst (cffi:foreign-alloc ,(parse-alien-type type))))
        ,@ (loop for (a b) on inits by #'cddr
                 collect 
                 `(setf (slot ,inst ',(parse-alien-type a)) ,b))
        ,inst))))
(defgluecode (:export :gtk)
  (defun struct-free (x) (cffi:foreign-free x)))
(defgluecode (:export :gtk)
  (defun cstring->string (ptr)
    (cffi:foreign-string-to-lisp ptr)))
(defgluecode (:export :gtk)
  (defun string->cstring (str)
    (cffi:foreign-string-alloc str)))
(defgluecode (:export :gtk)
  (defun cstring-free (gstr) gstr (values)))
(defgluecode (:export nil)
  (defun parse-alien-type (k)
    ;; unexported helper to parse OpenMCL-style foreign typenames like
    ;; :<G>tk<W>idget
    (cond ((consp k)
           (cons (parse-alien-type (car k))
                 (parse-alien-type (cdr k))))
          ((eql k t) t)
          ((keywordp k) k)
          ((symbolp k)
           (if (eql (symbol-package k)
                    (find-package :gtk))
             k
             (intern (string k) :gtk)))
          (t (error "\"Not an alien type: ~S.\"" k)))))

(defgluecode (:export :gtk)
  (defmacro define-signal-handler (name return params &body body)
    (let ((args (loop for p in params
                      collect 
                      (if (consp p)
                        (list (first p)
                              (parse-alien-type (second p)))
                        `(,p :pointer))))
          (type (parse-alien-type return)))
      `(cffi:defcallback ,name ,type ,args ,@body))))

(defgluecode (:export :g)
  (defmacro "g-callback" (x)
    `(cffi:callback ,x)))

(defgluecode (:export :g)
  (defun "g-signal-connect" (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                             ("g-nullptr") 0)))
(defgluecode (:export :g)
  (defun "g-signal-connect-after"
      (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                         ("g-nullptr") 1)))

(defgluecode (:export :g)
  (defun "g-signal-connect-swapped"
      (instance detailed_signal c_handler data)
    ("g-signal-connect-data" instance detailed_signal c_handler data 
                           ("g-nullptr") 2)))


(defparameter postlude-string
  "
#+:sbcl
(declaim (sb-ext:unmuffle-conditions style-warning 
                                     sb-ext:compiler-note))
")

;;;
;;; Output routines
;;;

(defun output-types (fil)
  ;; header lets us output across multiple files if we want.
  (let ((*print-case* ':downcase))
    (loop with i = most-negative-fixnum
          for e in gtk-entries
          for j = (ffi-entry-number e)
          for d = (ffi-entry-def e)
          for n = (ffi-entry-name e)
          do
          ;; print comment between classified sections of file
          (cond ((and (< i class-constant) (class-constant? e))
                 (format fil "~%~%;;; Constants~%"))
                ((and (< i class-ctype) (class-ctype? e))
                 (format fil "~%~%;;; C types~%"))
                ((and (< i class-pointer) (class-pointer? e))
                 (format fil "~%~%;;; Pointer types~%"))
                ((and (< i class-basic) (class-basic? e))
                 (format fil "~%~%;;; Gtk types~%"))
                ((and (< i class-struct1) (class-struct1? e))
                 (format fil "~%~%;;; Structs~%"))
                ((and (< i class-struct2) (class-struct2? e))
                 (format fil "~%~%;;; Structs that reference structs~%"))
                )
          (case (car d)
            (( type )
             (let ((form (cffi-type-def e)))
               (pprint (cssym-list! form) fil)))
            (( struct union )
             ;; turn off lispify to avoid inadvertent renamaing
             (let ()
             (pprint (cssym-list! (sbcl-struct-def e))
                     fil)))
            (( enum )
             (dolist (x (cssym-list! (enum-constants d )))
               (gtk-export (second x))
               (pprint x fil)))
            )
          (setf i (ffi-entry-number e)))
    (values)))

;;;
;;; make the FFI:

(defun lambda-gtk (p &optional (lib-packaging t))
  (gtk-ffi-init)
  (format t "~%loading api...")
  (mapdefs #'doapi (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                  :name "gtk.api"))
  (format t "~%loading ffi...")
  (mapdefs #'dodef (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                  :name "gtk.ffi"))
  (format t "~%generating entries...")
  (force-output)
  (activate-entries)
  (classify-entries)
  (sort-struct2s)
  (setf gtk-entries (gtk-entries))
  (when p
    (format t "~%writing output in ~s..." p)
    (force-output)
    (outfile (f p)
             (output-prelude f)
             (output-packaging f)
             (output-gluecode f)
             (output-types f)
             (output-accessors f)
             (output-functions f)
             (output-postlude f)
             ))
  (format t "~%Done.")
  t)
