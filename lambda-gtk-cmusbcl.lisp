;;; generates gtk interface file for CMU or SBCL:
;;;   $ cd /path/to/lambda-gtk
;;;   $ sbcl
;;;   ? (load "lambda-gtk-cmusbcl")
;;;   ? (lambda-gtk "gtkffi-cmusbcl.lisp")
;;;   ? (compile-file "gtkffi-cmusbcl")
;;;   ? (load "gtkffi-cmusbcl")

(in-package :lambda-gtk)

;; (eval-when (:load-toplevel :compile-toplevel :execute)
;;   (load (make-pathname :name "lambda-gtk-common" :type nil
;;                        :defaults *load-pathname*)))

;;;
;;; entrys from the .ffi file are shifted to specific sections in
;;; the output file:

(defconstant class-constant 10000) ; enum constants
(defconstant class-ctype    20000) ; refer to int, char etc.
(defconstant class-pointer  30000) ; pointer types 
(defconstant class-basic    40000) ; refer to c-type or pointers
(defconstant class-struct1  50000) ; refer to ctypes pointers or basic
(defconstant class-struct2  60000) ; refer to other structs
(defconstant class-accessor 70000) ; struct slot accessors
(defconstant class-function 90000) ; function entry
(defconstant class-macro    100000) ; macro entry

(defun class-unknown? (e)
  (let ((n (ffi-entry-number e)))
    (< n class-constant)))
(defun class-constant? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-constant n) (< n class-ctype)
         class-constant)))
(defun class-ctype? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-ctype n) (< n class-pointer)
         class-ctype)))
(defun class-pointer? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-pointer n) (< n class-basic))))
(defun class-basic? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-basic n) (< n class-struct1)
         class-basic)))
(defun class-struct1? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct1 n) (< n class-struct2)
         class-struct1)))
(defun class-struct2? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct2 n) (< n class-accessor)
         class-struct2)))
(defun class-struct? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-struct1 n) (< n class-accessor))))
(defun class-accessor? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-accessor n) (< n class-function))))
(defun class-function? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-function n) (< n class-macro)
         class-function)))
(defun class-macro? (e)
  (let ((n (ffi-entry-number e)))
    (and (<= class-macro n) class-macro)))

(defparameter ctype-mappings
  ;; map basic ffi types to sbcl foreign types
  `(
    ( (char ()) char)
    ( (signed-char ()) char)
    ( (short ()) short)
    ( (int ())   int)
    ( (long ()) long)
    ( (unsigned-char ()) unsigned-char)
    ( (unsigned-short ()) unsigned-short)
    ( (unsigned-int ()) unsigned-int)
    ( (unsigned ()) unsigned-int)
    ( (unsigned-long ()) unsigned-long)
    ( (float ()) float)
    ( (double ()) double)
    ( (long-long ()) (integer 64))
    ( (unsigned-long-long ()) (unsigned 64))
    ( (void ()) void)

    ( (pointer (void ()))  (* t))
    ( (pointer (char ()))  c-string  )
    ( (pointer (gchar ()))  c-string  )
    ( (pointer (guchar ()))  c-string  )

    ))

(defun sbcl-type (x &optional *t)
  ;; return an sbcl alien type specifier for a ffi type decl
  ;; if *t is t then force (pointer ...) to (* t)
  (cond ((null x)
         nil)
        ((eql (car x) 'pointer)
         ;; check if common pointer type
         (or (ctype? x)
             ;; check pointer's data
             (let ((res (sbcl-type (cadr x) *t)))
               (if res
                 ;; return (* t) if a pointer to a pointer
                 (if (and (consp res)
                          (eql (car res) '*))
                   (list '* t)
                   (if *t (list '* t)
                       (list '* res)))
                 nil))))
        ((eql (car x) 'typedef)
         (cadr x))
        ((member (car x) '(struct-ref union-ref))
         ;; Add "s" or "u" to numeric names.
         (let* ((s (eq (car x) 'struct-ref))
                (n (if (digit-char-p (elt (cadr x) 0))
                     (format nil "_~:[u~;s~]~A" s (cadr x))
                     (format nil "_~A" (cadr x)))))
           ;; wrap in (struct ...) or (union ...)
           (list (if s 'struct 'union) n)))
        ((eql (car x) 'function)
         (list '* t))
        ((eql (car x) 'array)
         ;; dont wrap (struct ...) around typdefs
         (if (eql (car (third x)) 'typedef)
           (list 'array (second (third x)) (second x))
           (list 'array (sbcl-type (third x) *t) (second x))))
        ((eql (car x) 'enum-ref) 'int)
        (t (ctype? x))))

(defun atomic-def? (def &optional grovel?)
  ;; def is "atomic" if it does not reference any structs.  atomics
  ;; appear in the output file before any struct definitions. if
  ;; grovel? is true then def is a stuct slot's type expresssion.  in
  ;; this case we descend into the type list and check all refs.
  (labels ((checkfail (x)
             (cond ((consp x)
                    (or (checkfail (car x))
                        (checkfail (cdr x))))
                   ((stringp x)
                    (let ((d (get-def x)))
                      (if (not d) t
                          (if (atomic-def? d) nil t))
                      ))
                   (t nil))))
    (if grovel?
      (if (checkfail def) nil t)
      (case (car def)
        (type
         (let ((x (fourth def)))
           (case (car x)
             (( struct-ref union-ref ) nil)
             (( struct union ) nil)
             (( enum-ref ) t)
             (t 
              ;; grovel through function and pointer type specs
              ;; and check embedded typedefs...
              (if (checkfail x) nil t)))))
        (( function macro )  nil )
        ((struct union) nil)
        ((enum-ref ) t)
        (t t)))))

(defun atomic-struct? (def)
  ;; return t if struct's slots only references atomic types or
  ;; pointers (which are translated as (* t) in the output file)
  (let ((slots (fourth def)))
    (loop for slot in slots
          for type = (second (second slot))
          always (or (eql (car type) 'pointer)
                     (atomic-def? (second (second slot)) t)))))

;;;
;;; api and ffi loading routines
;;;

(defun classify-entries ()
  ;; classify entry types so that we can resolve type dependancies
  ;; in the output FFI file
  (maphash (lambda (k e)
             k
             (when (ffi-entry-include e)
               ;; skip if already classified...
               (unless (> (ffi-entry-number e) class-constant)
                 (let ((d (ffi-entry-def e)))
                   (case (car d)
                     (( type )
                      (cond ((struct-type-def? d)
                             ;;GtkAllocation=GdkRectangle
                             (incf (ffi-entry-number e) class-struct2))
                            ((pointer-def? d)
                             (incf (ffi-entry-number e) class-pointer))
                            ((enum-ref-def? d)
                             (incf (ffi-entry-number e) class-ctype))
                            ((ctype? (fourth d))
                             (incf (ffi-entry-number e) class-ctype))
                            ((atomic-def? d)
                             (incf (ffi-entry-number e) class-basic))
                            ((eql (car (fourth d)) 'typedef)
                             ;; (type foo (typedef bar))
                             (let* ((s (gethash (cadr (fourth d)) 
                                                gtk-ffi))
                                    (n (and s (or (class-struct1? s)
                                                  (class-struct2? s)))))
                               (if n
                                 (incf (ffi-entry-number e)
                                       n)
                                 (warn "unresolved typedef class: ~S" e))))
                            (t
                             (warn "Failed to classify: ~s" d))))
                     (( struct union )
                      (if (atomic-struct? d)
                        (incf (ffi-entry-number e) class-struct1)
                        (incf (ffi-entry-number e) class-struct2)))
                     (( enum )
                      (incf (ffi-entry-number e) class-constant))
                     (( function )
                      (incf (ffi-entry-number e) class-function))
                     (( macro )
                      (incf (ffi-entry-number e) class-macro))
                     (t (format t "~%Failed to classify: ~s" d)))))))
           gtk-ffi))

(defun activate-struct-slots (entry)
  (let ((def (ffi-entry-def entry))
        (bitfields (list))
        (include (list )))
    ;; check for bitfield slots
    (when (eql (ffi-entry-include entry) ':api)
      (let ((top nil)
            (bfs nil))
        (mapslots
         def
         (lambda (name type bf)
           (if bf 
             (progn
               (when (not bfs)
                 (setq top (list (second type) 0)
                       bfs (list top)))
               (push name bfs)
               (incf (second top) (second bf))
               ;;(unless (< (second top) 32)
               ;;  (warn "~A.~A: BITFIELD WITDH ~S WIDER THAN 32 BITS."
               ;;        (def-name def) name (second top)))
               )
             (if bfs
               (progn (push (nreverse bfs) bitfields)
                      (setq bfs nil top nil))))))
      ;; end slot may be bitfield...
      (when bfs (push (nreverse bfs) bitfields))
      (when bitfields
        (push (cons :bitfields (reverse  bitfields))
              (ffi-entry-data entry)))))

    ;; check for included slots
    (mapslots def
              (lambda (name type bitfield?) 
                bitfield?
                (let ((d (struct-slot? name type)))
                  (if d (push (list name d) include)))))
    ;; include is a list of (slot def) where each def is a structure
    ;; that slot inlines.
    (when include
      (loop for i in include
         for d = (second i)
         for n = (def-name d)
         for e = (get-entry n)
         do
         (progn (unless (ffi-entry-include e)
                  ;; include if ffi but no API accessors.
                  (setf (ffi-entry-include e) T)
                  ;; check slots recursively
                  (activate-struct-slots e)
                  )))
      (setq include (reverse include))
      ;; list of inlined entries: ("slotname" "structname")
      ;; CAREFUL: the inlined struct may be in an array!
      (push (cons :included include)
            (ffi-entry-data entry)))))

; (activate-struct-slots (get-def "_GtkWidget"))
                
(defun activate-entries ()
  ;; map over the ffi entries and mark only API entries for inclusion
  ;; in the output file. This recurses on slot types for every
  ;; included struct.  Entries included from the API are marked by the
  ;; symbol ':api, entries included from recursive slot inspection are
  ;; marked by 't.
  (maphash
   (lambda (key entry)
     (let* ((def (ffi-entry-def entry))
            (nam (def-name def)))
       (case (car def)
         (( function )                  ; macro
          (unless (or (upper-case-name? nam)
                      (internal-name? nam))
            (when (gethash nam gtk-api)
              (setf (ffi-entry-include entry) :api))))
         (( struct union )
          (let ((e (gethash key gtk-api)))
            (when e
              (setf (ffi-entry-include entry) :api)
              (activate-struct-slots entry))))
         (( type )
          (cond ((gethash key gtk-api)
                 (setf (ffi-entry-include entry) :api)
                 ;; if its an enum typedef activate the enum itself...
                 (when (enum-ref-def? def)
                   (let ((enum (get-entry (second (fourth def)))))
                     (setf (ffi-entry-include enum) ':api))))
                ((pointer-def? def) 
                 (setf (ffi-entry-include entry) t))
                ((atomic-def? def)
                 (setf (ffi-entry-include entry) t))
                ;; type is typedef of a type (ie GdkAllocation)
                ((and (eql (car (fourth def)) 'typedef)
                      (ffi-entry-include 
                       (get-entry (second (fourth def)))))
                 (setf (ffi-entry-include def) t))
                (t nil)))
         ;;(( enum )
         ;; (dolist (d (fourth def))
         ;;   (when (eql (car (gethash (car d) gtk-api)) ':constant)
         ;;     (setf (ffi-entry-include entry) :api)
         ;;     (return))))
         )))
   gtk-ffi))

(defun sort-struct2s ( )
  (let ((struct2s (list))
        (entrynum (+ class-struct2 1)))
    ;; collect all the struct2s
    (maphash (lambda (k v) 
               k
               (when (class-struct2? v)
                 (push v struct2s)))
             gtk-ffi)
    ;; sort structs so includers are later than includees
    (flet ((struct-not-included (a b)
             ;; true if a is not in b's :included list
             (let ((n (ffi-entry-name a)))
               (loop for x in (cdr (assoc ':included
                                          (ffi-entry-data b)))
                     ;; each x is ("slot" (struct ...))
                     never (equal n (def-name (second x)))))))
      (setf struct2s 
            (sort struct2s
                  (lambda (a b)
                    (if (struct-not-included a b)
                      a
                      b)))))
    ;; set ffi entry order to sorted order.
    (dolist (s struct2s)
      (setf (ffi-entry-number s) entrynum)
      (incf entrynum))
    t))

;;;
;;; definition routines
;;;

(defun sbcl-struct-def (entry)
  ;; add "s" or "u" to unnamed structs
  (let* ((def (ffi-entry-def entry))
         ;; add "s" or "u" to struct/union numbers
         (name (if (digit-char-p (elt (third def) 0))
                 (format nil "~:[u~;s~]~A" (eq (car def) 'struct)
                         (third def))
                 (third def)))
         (bfnum 0)
         (slots (list)))
    (mapslots def
              (lambda (slot type bf)
                (if bf
                  ;; output a bitfield padding slot for the
                  ;; first field in each bitfield.
                  (if (eql (first bf) 0)
                    (push (list (format nil "bf_padding~D"
                                        (incf bfnum))
                                (second type))
                          slots)
                    nil) ;else skip bitfield slot
                  ;; rewrite pointer types to (* t). if type is array
                  ;; then do the same to the array's type
                  (push
                   (list slot
                         (let ((typ (sbcl-type type)))
                           (if (and (consp typ) (eql (car typ) '*))
                             (list '* t)
                             (if (and (consp typ) (eql (first typ) 'array)
                                      (consp (second typ))
                                      (eql (first (second typ)) '*))
                               (progn (setf (second typ) (list '* t))
                                      typ)
                               typ))))
                   slots))
                ))
    `(defalien-type ,name
         ,(list* (car def) 
                 (format nil "_~A" name)
                 (nreverse slots)))))
                          
(defun sbcl-type-def (entry)
  (let* ((def (ffi-entry-def entry))
         (res (if (pointer-def? def)
                (list '* t)
                (sbcl-type (fourth def)))))
    ;; (define-alien-type foo void) is error
    (when (eql res 'void)
      (setf res '(struct nil)))
    `(defalien-type , (third def) ,res)))

(defun function-arg-type (arg)
  ;; retuns a list ({type} [:in-out])
  (if (eql (car arg) 'pointer)
    (let ((isa (sbcl-type arg)))
      (if (or (eql isa 'c-string)
              (equal isa '(* T)))
        (list isa)
        (let ((x (second isa)))
          ;;(print (list :x-> x))
          (cond ((symbolp x) ; basic type
                 (if (eql x 'c-string)
                   (list (list '* t))
                   (list x ':in-out)))
                ((stringp x)
                 (let ((e (get-entry x)))
                   (if (and e (class-ctype? e))
                     (list x ':in-out)
                     (list (list '* t)))))
                (t ;;(format t "~%unanticipated type: ~S -> ~S" arg isa)
                   (list (list '* t)))))))
    (list (sbcl-type arg))))

; (function-arg-type '(pointer (typedef "gint")))    ((* T) )
; (function-arg-type '(pointer (typedef "GList")))   ((* T) )
; (function-arg-type '(pointer (int ())))            (INT :in-out)
; (function-arg-type '(pointer (typedef "gint")))    ((* t))
; (function-arg-type '(pointer (typedef "gdouble"))) ((* t))
; (function-arg-type '(typedef "gint"))              ("gint" )
; (function-arg-type '(pointer (char ())))           (CSTRING )
; (function-arg-type '(char ()))                     (CHAR )
; (function-arg-type '(typedef "gpointer"))          ("gpointer" )
; (function-arg-type '(pointer (pointer (typedef "PangoAttrList"))))   ((* t) )
; (function-arg-type '(pointer (pointer (char ())))) ((* T) )
; (function-arg-type '(pointer (typedef "GdkAtom"))) ((* T) )

(defun function-def-args (args)
  ;; gather all args until the first 'void' arg
  (loop for a in args
        for p in '(a b c d e f g h i j k l m n o p q r s u v w x y z)
        for  x = (function-arg-type a)
        until (eql (car x) 'void)
        collect (cons p x)))

(defun pass-value (arg)
  ;; wrap basic arg types in expressions to coerce lisp values to that
  ;; type
  (let ((typ (second arg)))
    (cond ((equal typ "gboolean")
           (gtk-boolean (first arg)))
          ((find typ '("double" "gdouble") :test #'equal)
           (gtk-double  (first arg)))
          ((find typ '("float" "gfloat") :test #'equal)
           (gtk-single (first arg)))
          (t
           (first arg)))))

(defun return-value (call type args)
  (if (equal type "gboolean")
    (if (find ':in-out args :test #'member)
      (let ((var (gentemp "V")))
        `(let ((,var (multiple-value-list ,call)))
          (apply #'values (if (= 1 (car ,var)) t nil) ;+gtk-true+
           (cdr ,var))))
      (let ((var (gentemp "V")))
        `(let ((,var ,call))
          (if (= ,var 1) t nil)))) ; +gtk-true+
    call))
        
(defun wrapper-def (def)
  (let* ((cname (third def))
         (lname (lisp-name cname))
         (args (function-def-args (second (fourth def))))
         (pars (loop for p in args collect (car p)))
         (retn (first (function-arg-type (third (fourth def)))))
         (call (list* (format nil "|~A|" cname)
                      (loop for a in args collect (pass-value a))
                      )))
    `(progn 
      (defalien ,(format nil "~S" cname) , retn ,@args)
      (defun ,lname ,pars , (return-value call retn args)))))

;;;
;;; accessors
;;; 

(defun struct-slot-accessor (struct arry? slot &optional slot2)
  ;; if arry? is true its the dimension of the array in slot
  ;; slot = (name type)
  (let* ((typ `(* , struct))
         (nam (if slot2 
                (struct-accessor-name struct (first slot) (first slot2))
                (struct-accessor-name struct (first slot))))
         (cst `(if (typep ptr '(alien ,typ))
                     ptr (cast ptr ,typ)))
         (var (gentemp "V"))
         (exp `(slot ,var ',(first slot)))
         ;; wrap the user's value just like regular funtions in api.
         (val
          (pass-value
           (cons 'val 
                 (function-arg-type 
                  (if slot2 (second slot2)
                      (second slot)))))))
    ;; if its an arry wrap the deref around slot access
    (if arry? (setq exp `(deref ,exp index)))
    ;; wrap slot value with included slot's access
    (if slot2 (setq exp `(slot ,exp ',(first slot2))))
    `(defun ,nam (ptr ,@ (if arry? '(index) nil)
                  &optional (val nil vp))
      (let ((,var ,cst))
        (declare (type (alien ,typ) ,var))
        ;; add bounds checking if array
        ,@ (if arry? 
             `((unless (< index ,arry?)
                 (error ,(format nil
                                 "\"struct array index ~~D more than ~D.\""
                                 arry?) index)))
             (list))
        (if vp
          (setf ,exp , val)
          ,exp)))))

; (cssym-list! (struct-accessor-defs (get-entry "GdkColor")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkGammaCurve")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkAdjustment")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkWidget")))
; (cssym-list! (struct-accessor-defs (get-entry "GtkStyle")))

;; handcoded entries and glue code

(defparameter prelude-string "
(in-package :lambda-gtk)
")

;;;
;;; define the gluecode. strings are replaced by cssyms.

(defgluecode (:export nil)
  (declaim "#+:cmu"  (optimize ("extensions:inhibit-warnings" 3))
           "#+:sbcl" ("sb-ext:muffle-conditions" style-warning 
                                                "sb-ext:compiler-note")))
(defgluecode (:export :gtk) (defconstant "+gtk-false+" 0))
(defgluecode (:export :gtk) (defconstant "+gtk-true+" 1))
(defgluecode (:export :g)
  (defun "g-nullptr" ()
     "#+:sbcl (sb-sys:int-sap 0)"
     "#+:cmu  (system:int-sap 0)" ))
(defgluecode (:export :g)
  (defun "g-nullptr?" (p)
    (if (typep p '(alien (* t))) 
      ";; then"
      "#+:sbcl (null-alien p)"
      "#+:cmu  (zerop (system:sap-int (alien-sap p)))"
      ";; else"
      "#+:sbcl (zerop (sb-sys:sap-int p))"
      "#+:cmu  (zerop (system:sap-int p))")))
(defgluecode (:export nil) (defvar *gtk-init* nil))
(defgluecode (:export :gtk)
  (defun "gtk-init-ensure" (&optional strings)
    (declare (ignore strings))
    (unless *gtk-init*
      ("gtk-init" 0 ("g-nullptr"))
      (setq *gtk-init* t))
    *gtk-init*)  )
(defgluecode (:export nil)
  (defmacro defalien (name return &rest args)
    (let* ((l (if (consp name) name (list name (intern name))))
           (s (second l)))
      "#+:sbcl"
      `(progn (declaim (inline ,s))
              (define-alien-routine ,l ,return ,@ args))
         "#+:cmu"
      `(progn (declaim (inline ,s))
              (def-alien-routine ,l ,return ,@ args)))))
(defgluecode (:export nil)
  (defmacro defalien-type (name type)
    "#+:sbcl" `(define-alien-type ,name ,type)
    "#+:cmu" `(def-alien-type ,name ,type)))
(defgluecode (:export :gtk)
  (defmacro struct-alloc (type &rest inits)
    (let ((inst (gensym)))
      `(let ((,inst (make-alien ,(parse-alien-type type))))
        ,@ (loop for (a b) on inits by #'cddr
                 collect 
                 `(setf (slot ,inst ',(parse-alien-type a)) ,b))
        ,inst))))
(defgluecode (:export :gtk)
  (defun struct-free (x) (free-alien x)))
(defgluecode (:export :gtk)
  (defun cstring->string (ptr)
    (if ("g::nullptr?" ptr) 
        nil
        (let ((p (if (typep ptr '(alien (* (unsigned 8))))
                     ptr (cast ptr (* (unsigned 8))))))
          (declare (type (alien (* (unsigned 8))) p))
          (with-output-to-string (s)
            (loop for i from 0
                  for c = (deref p i)
                  until (zerop c)
                  do (write-char (code-char c) s)))))))
(defgluecode (:export :gtk)
  (defun string->cstring (str)
    (if (= (length str) 0)
      (sap-alien "#+:cmu" ("system:int-sap" 0)
                 "#+:sbcl" ("sb-sys:int-sap" 0)
                 (* (unsigned 8)))
      (let* ((size (length str))
             (cstr (make-alien (unsigned 8) (1+ size))) 
             (str* (cast cstr (* (unsigned 8)))))
        (dotimes (i size)
          (setf (deref cstr i) (char-code (char str i))))
        (setf (deref cstr size) 0)
        str*))))
(defgluecode (:export :gtk)
  (defun cstring-free (gstr) gstr (values)))
(defgluecode (:export nil)
  (defun parse-alien-type (k)
    ;; unexported helper to parse OpenMCL-style foreign typenames like
    ;; :<G>tk<W>idget
    (cond ((consp k)
           (cons (parse-alien-type (car k))
                 (parse-alien-type (cdr k))))
          ((eql k t) t)
          ((keywordp k)
           (let ((s (string k)))
             (if (find #\< s)
               (intern (delete-if (lambda (x) (member x '(#\< #\>)))
                                  s)
                       :gtk)
               (intern s :gtk))))
          ((symbolp k)
           (if (eql (symbol-package k)
                    (find-package :gtk))
             k
             (intern (string k) :gtk)))
          (t (error "\"Not an alien type: ~S.\"" k)))))
(defgluecode (:export :gtk)
  (defmacro define-signal-handler (name return params &body body)
    (let ((args (loop for p in params
                      collect 
                      (if (consp p)
                        (list (first p)
                              (parse-alien-type (second p)))
                        `(,p (* t)))))
          (type (parse-alien-type return)))
      #+:sbcl
      `(defparameter ,name 
        (sb-alien::alien-sap
         (sb-alien::alien-lambda ,type ,args ,@body)))
      #+:cmu
      `(def-callback ,name ,(cons type args) ,@body))))
(defgluecode (:export :g)
  (defmacro "g-callback" (x)
    "#+:sbcl" x
    "#+:cmu `(alien::callback ,x)"))
(defgluecode (:export :g)
  (defun "g-signal-connect" (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                             ("g-nullptr") 0)))
(defgluecode (:export :g)
  (defun "g-signal-connect-after"
      (instance detailed-signal c-handler data)
    ("g-signal-connect-data" instance detailed-signal c-handler data
                         ("g-nullptr") 1)))
(defgluecode (:export :g)
  (defun "g-signal-connect-swapped"
      (instance detailed_signal c_handler data)
    ("g-signal-connect-data" instance detailed_signal c_handler data 
                           ("g-nullptr") 2)))

(defparameter postlude-string
  "
#+:sbcl
(declaim (sb-ext:unmuffle-conditions style-warning 
                                     sb-ext:compiler-note))
")

;;;
;;; Output routines
;;;

(defun output-types (fil)
  ;; header lets us output across multiple files if we want.
  (let ((*print-case* ':downcase))
    (loop with i = most-negative-fixnum
          for e in gtk-entries
          for j = (ffi-entry-number e)
          for d = (ffi-entry-def e)
          for n = (ffi-entry-name e)
          do
          ;; print comment between classified sections of file
          (cond ((and (< i class-constant) (class-constant? e))
                 (format fil "~%~%;;; Constants~%"))
                ((and (< i class-ctype) (class-ctype? e))
                 (format fil "~%~%;;; C types~%"))
                ((and (< i class-pointer) (class-pointer? e))
                 (format fil "~%~%;;; Pointer types~%"))
                ((and (< i class-basic) (class-basic? e))
                 (format fil "~%~%;;; Gtk types~%"))
                ((and (< i class-struct1) (class-struct1? e))
                 (format fil "~%~%;;; Structs~%"))
                ((and (< i class-struct2) (class-struct2? e))
                 (format fil "~%~%;;; Structs that reference structs~%"))
                )
          (case (car d)
            (( type )
             (let ((form (sbcl-type-def e)))
               (pprint (cssym-list! form) fil)))
            (( struct union )
             ;; turn off lispify to avoid inadvertent renamaing
             (let ()
             (pprint (cssym-list! (sbcl-struct-def e))
                     fil)))
            (( enum )
             (dolist (x (cssym-list! (enum-constants d )))
               (gtk-export (second x))
               (pprint x fil)))
            )
          (setf i (ffi-entry-number e)))
    (values)))

;;;
;;; make the FFI:

(defun lambda-gtk (p &optional (lib-packaging t))
  (gtk-ffi-init)
  (format t "~%loading api...")
  (mapdefs #'doapi (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                  :name "gtk.api"))
  (format t "~%loading ffi...")
  (mapdefs #'dodef (make-pathname :directory (pathname-directory *lambda-gtk-directory*)
                                  :name "gtk.ffi"))
  (format t "~%generating entries...")
  (force-output)
  (activate-entries)
  (classify-entries )
  (sort-struct2s )
  (setf gtk-entries (gtk-entries))
  (when p
    (format t "~%writing output in ~s..." p)
    (force-output)
    (outfile (f p) 
             (output-prelude f)
             (output-packaging f)
             (output-gluecode f)
             (output-types f)
             (output-accessors f)
             (output-functions f)
             (output-postlude f)))
  (format t "~%Done.")
  t)



